# README #

### What is this repository for? ###

This is a demo Angular app that implement D3 chart on top of "Tour of Heroes" tutorial app.

### How do I get set up? ###

1. Clone the repository
2. Navigate to project folder
3. run `yarn`
4. run `ng serve --open`
