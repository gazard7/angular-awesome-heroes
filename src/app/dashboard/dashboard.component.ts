import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import countBy from 'lodash/countBy'

declare let d3: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['../../../node_modules/nvd3/build/nv.d3.css', './dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  options: Object = {};
  data: any[] = [];

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.options = {
      chart: {
        type: 'pieChart',
        height: 500,
        x: function(d){return d.key;},
        y: function(d){return d.y;},
        showLabels: true,
        duration: 500,
        labelThreshold: 0.05,
        // labelType: 'percent',
        donut: true,
        donutRatio: 0.35,
        // labelSunbeamLayout: true,
        legend: {
          margin: {
            top: 5,
            right: 35,
            bottom: 5,
            left: 0
          }
        }
      }
    };
    this.getCharData();
  }

  getCharData(): void {
    this.heroService.getHeroes()
    .subscribe(heroes => {
      let sumObj = countBy(heroes, function(h) {
        return h.awesomeness
      });
      this.data = Object.keys(sumObj).map(function(key) {
        return { key: key, y: sumObj[key] };
      });
    })
  }
}
