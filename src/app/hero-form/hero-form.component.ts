import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Hero }    from '../hero';
import startCase from 'lodash/startCase'
import toLower from 'lodash/toLower'

@Component({
  selector: 'app-hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.css']
})
export class HeroFormComponent {
  @Input() model: Hero;
  @Output() saveEvent = new EventEmitter<Hero>();
  @Output() backEvent = new EventEmitter<void>();

  awesomenessLevel = ['Awesome', 'Superb', 'Not Bad', 'Meh', 'Hopeless'];

  constructor() { }

  onSubmit() {
    let hero: Hero = this.model
    hero.name = startCase(toLower(hero.name));
    this.saveEvent.emit(hero);
  }

  goBack(): void {
    this.backEvent.emit();
  }

}
