import { InMemoryDbService } from 'angular-in-memory-web-api';
import countBy from 'lodash/countBy';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 11, name: 'Vision', awesomeness: 'Meh' },
      { id: 12, name: 'Narco', awesomeness: 'Not Bad' },
      { id: 13, name: 'Bombasto', awesomeness: 'Awesome' },
      { id: 14, name: 'Celeritas', awesomeness: 'Not Bad' },
      { id: 15, name: 'Magneta', awesomeness: 'Awesome' },
      { id: 16, name: 'Rubber Man', awesomeness: 'Not Bad' },
      { id: 17, name: 'Dynama', awesomeness: 'Not Bad' },
      { id: 18, name: 'Dr IQ', awesomeness: 'Superb' },
      { id: 19, name: 'Magma', awesomeness: 'Hopeless' },
      { id: 20, name: 'Tornado', awesomeness: 'Not Bad' }
    ];
    // let sumObj = countBy(heroes, function(h) {
    //   return h.awesomeness
    // });
    // // console.log(sumObj);
    // let chartData = Object.keys(sumObj).map(function(key) {
    //   return { key: key, y: sumObj[key] };
    // });
    
    return {heroes};
  }
}
