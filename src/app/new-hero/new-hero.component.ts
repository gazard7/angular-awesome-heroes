import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import { Hero } from '../hero';
import { HeroService }  from '../hero.service';

@Component({
  selector: 'app-new-hero',
  templateUrl: './new-hero.component.html',
  styleUrls: ['./new-hero.component.css']
})
export class NewHeroComponent implements OnInit {
  hero: Hero = new Hero();

  constructor(
      private heroService: HeroService,
      private router: Router
    ) {}

  ngOnInit() {
  }

  goBack(): void {
    this.router.navigate(['/heroes']);
  }

   save(hero: Hero): void {
    this.heroService.addHero(hero)
      .subscribe(() => this.goBack());
  }

}
